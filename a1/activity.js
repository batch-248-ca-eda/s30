// A.

db.fruits.aggregate(
	[
		{$match: 
			{
				onSale: true,
				stock: {$gte: 20}
			}
		},
		{$count: "enoughStock"}
	]
);



// B.

db.fruits.aggregate(
	[
		{$match: 
			{
				onSale: true,
			}
		},

		{$group:
			{
				_id: "$supplier_id",
				avg_price: {$avg: "$price"}
			}
		},
        
        {$sort:
			{
				_id: -1
			}
		}
	]
);



// C.

db.fruits.aggregate(
	[
		{$match: 
			{
				onSale: true
			}
		},

		{$group: 
			{
				_id: "$supplier_id", 
				max_price: {$max: "$price"}
			}
		},

		{$sort:
			{
				_id: -1
			}
		}

	]
);



// D.

db.fruits.aggregate(
	[
		{$match: 
			{
				onSale: true
			}
		},

		{$group: 
			{
				_id: "$supplier_id", 
				min_price: {$min: "$price"}
			}
		},

		{$sort:
			{
				_id: 1
			}
		}
	]
);

