db.fruits.insertMany([
            {
                name : "Apple",
                color : "Red",
                stock : 20,
                price: 40,
                supplier_id : 1,
                onSale : true,
                origin: [ "Philippines", "US" ]
            },

            {
                name : "Banana",
                color : "Yellow",
                stock : 15,
                price: 20,
                supplier_id : 2,
                onSale : true,
                origin: [ "Philippines", "Ecuador" ]
            },

            {
                name : "Kiwi",
                color : "Green",
                stock : 25,
                price: 50,
                supplier_id : 1,
                onSale : true,
                origin: [ "US", "China" ]
            },

            {
                name : "Mango",
                color : "Yellow",
                stock : 10,
                price: 120,
                supplier_id : 2,
                onSale : false,
                origin: [ "Philippines", "India" ]
            }
        ]);




	// MongoDB Aggregation

		// an aggregate is a cluster of things that have come or have been brought together
		// In MongoDB, aggregation operations group values from multiple documents together


	db.fruits.aggregate([
		{$match: { onSale: true } },
		{$group: { _id: "$supplier_id" , total: {$sum: "$stock"} } }
	]); 

	// Aggregation Pipeline Stages
		// Aggregation is typically done in 2-3 steps. Each process in aggregation is called a stage

		/*
			$match - is used to match or get documents which SATISFIES the condition 
			// Syntax : {$match: {fiuld:<value>}};

			$group - allows us to group together documents and create an analysis out of the grouped documents

			_id: in the group stage, essentially associates and id to our results
			_id: also, it determines the number of groups



		*/
		// Field projection

		db.fruits.aggregate([
			{$match: { onSale: true } },
			{$group: { _id: "$supplier_id" , total: {$sum: "$stock"} } },
			{$project: { _id: 0} }
		]);

		/*
			the $project can be used hen aggregating data to inlude/exclude fields from the returned results
			Syntax:
			{$project: {field: 1/0}}

			1 shows up
			0 does not show up

		*/

		// Sort aggregated results


		db.fruits.aggregate([
			{$match: { onSale: true } },
			{$group: { _id: "$supplier_id" , total: {$sum: "$stock"} } },
			{$sort: {total: -1}}
		]); 
		/*
			$sort can be used to change the order of the aggregated results

			Syntax:
			{$sort {field: 1/-1}}

			1 - ascending // 123456
			-1 - descending // 654321

		*/

		// aggregating results based on array fields

		db.fruits.aggregate([
			{$unwind: "$origin"}
		]);

		/*
			$unwind deconstructs an array field from a field with our array value to output a result for each element
			Syntax:

			{$unwind : field}
		*/

		db.fruits.aggregate([
			{$unwind: "$origin"},
			{$group: {_id: "$origin", kinds: {$sum: 1} } } 
		]);

		// MA1: find how many fruits

		db.fruits.aggregate(
			[
				{$match: { color : "Yellow"}},
				{$count: "yellowFruits"}
			]
		);


		// $count
			// Syntax
				// {$count: <string>}

		db.fruits.aggregate(
			[
				{$match: 
					{ 
						color : {$regex: 'yelloW', $options: '$i'}
					}
				},
				{$count: "yellowFruits"}
			]
		);

		// MA2: find how many fruit stocks that are less than or equal to 10

		db.fruits.aggregate(
			[
				{$match: 
					{
						stock: {$lte: 10} 
					} 
				},
				{$count: "stocks"}
			]
		);

		// MA3: find fruits on sale for each group of supplier that has the maximum stocks

		db.fruits.aggregate(
			[
				{$match: 
					{
						onSale: true
					}
				},

				{$group: 
					{
						_id: "$supplier_id", 
						max_stock: {$max: "$stock"}
					}
				}
			]
		);



		$match
		$group
		group them in supplier_id
		get the supplier_id stocks and count them